Usage:

This test content should be unaffected by the tools output using the update file method wih the output path set to testReadme.md

<!-- generation by terraform auto doc -->

Table of Contents
=================
1. [Terraform Requirements](#terraform-requirements)
1. [Terraform Providers](#terraform-providers)
1. [Terraform Variables](#terraform-variables)
1. [Terraform Data Sources](#terraform-data-sources)
1. [Terraform Resources](#terraform-resources)
1. [Terraform Modules](#terraform-modules)
1. [Terraform Outputs](#terraform-outputs)

# Terraform Requirements

| Name | Version |
| ---- | ------ |
| azuread | >= 1.18.0 |
| azurerm | >= 2.15.0 |
| null | >= 1.0.0 |
| random | >= 2.2.0 |
| terraform | >= 0.13 |

# Terraform Providers

| Name | Version | Code Postition |
| ---- | ------ | ------ |
| azuread | >= 1.18.0 | [main.tf: 13](/examples/full_test/main.tf#L13) |
| azurerm | >= 2.15.0 | [main.tf: 15](/examples/full_test/main.tf#L15) |
| null | >= 1.0.0 | [main.tf: 19](/examples/full_test/main.tf#L19) |
| tls |  | [main.tf: 11](/examples/full_test/main.tf#L11) |

# Terraform Variables

| Variable | Type | Description | Code Position |
| ---- | ------ | -------- | ------ |
| bool-1 | <pre> bool </pre> |  | [variables.tf: 1](/examples/full_test/variables.tf#L1) |
| long_type | <pre> object({<br>    name = string,<br>    foo  = object({ foo = string, bar = string }),<br>    bar  = object({ foo = string, bar = string }),<br>    fizz = list(string),<br>    buzz = list(string)<br>  }) </pre> | This description is itself markdown.<br>It spans over multiple lines.<br> | [variables.tf: 17](/examples/full_test/variables.tf#L17) |
| nt-1 |  | no type | [variables.tf: 12](/examples/full_test/variables.tf#L12) |
| string-1 | <pre> string </pre> | a test string | [variables.tf: 6](/examples/full_test/variables.tf#L6) |

# Terraform Data Sources

| Resource Name | Resource Type | Code Position |
| ---- | -------- | ------ |
| test | azurerm_resource_group | [main.tf: 15](/examples/full_test/main.tf#L15) |
| this_application | azuread_application | [main.tf: 13](/examples/full_test/main.tf#L13) |

# Terraform Resources

| Resource Name | Resource Type | Code Position |
| ---- | -------- | ------ |
| nulltest | null_resource | [main.tf: 19](/examples/full_test/main.tf#L19) |
| this | tls_private_key | [main.tf: 11](/examples/full_test/main.tf#L11) |

# Terraform Modules

| Module Name | Module Source | Module Location |
| ---- | -------- | ------ |
| fixture | test | [main.tf: 26](/examples/full_test/main.tf#L26) |
| test | testing | [main.tf: 21](/examples/full_test/main.tf#L21) |

# Terraform Outputs

| Output name | Description | Sensitive | Code Position |
| ---- | -------- | ---- | ------ |
| output-1 | test output 1 | false | [output.tf: 6](/examples/full_test/output.tf#L6) |
| unquoted | It's an unquoted output. | false | [output.tf: 1](/examples/full_test/output.tf#L1) |

<!-- generation by terraform auto doc end -->