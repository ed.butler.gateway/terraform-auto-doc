terraform {
  required_version = ">= 0.13"
  required_providers {
    random  = ">= 2.2.0"
    azurerm = ">= 2.15.0"
    azuread = ">= 1.18.0"
    null    = ">= 1.0.0"
  }
}

resource "tls_private_key" "this" {}

data "azuread_application" "this_application" {}

data "azurerm_resource_group" "test" {
  provider = "azurerm"
}

resource "null_resource" "nulltest" {}

module "test" {
  source  = "testing"
  version = "1.2.3"
}

module "fixture" {
  source  = "test"
  version = "4.5.6"
}