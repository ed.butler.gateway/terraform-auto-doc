package tables

import (
	"github.com/hashicorp/terraform-config-inspect/tfconfig"
	"io/ioutil"
	"path/filepath"
	"strconv"
	"strings"
	"testing"

	utils "terraform-auto-doc/internal/utils"
)

func TestMarkdownTable(t *testing.T) {
	tests := []struct {
		headings []string
		lengths  []string
		data     [][]string
		expected string
	}{
		{
			headings: []string{"Name", "Version"},
			lengths:  []string{"----", "------"},
			data:     [][]string{},
			expected: "| Name | Version |\n| ---- | ------ |",
		},
		{
			headings: []string{"Name", "Version"},
			lengths:  []string{"----", "------"},
			data: [][]string{
				[]string{"tricky\name", "\new.v1.20.0"},
			},
			expected: "| Name | Version |\n" +
				"| ---- | ------ |\n" +
				"| tricky<br>ame | <br>ew.v1.20.0 |", //  exisiting newlines \n should be replaced by <br>
		},
		{
			headings: []string{"Resource Name", "Resource Type", "Code Position"},
			lengths:  []string{"----", "--------", "------"},
			data: [][]string{
				[]string{"test", "string", "[main.tf: 1](main.tf#L1)"},
				[]string{"test2", "int", "[main.tf: 5](main.tf#L5)"},
			},
			expected: "| Resource Name | Resource Type | Code Position |\n" +
				"| ---- | -------- | ------ |\n" +
				"| test | string | [main.tf: 1](main.tf#L1) |\n" +
				"| test2 | int | [main.tf: 5](main.tf#L5) |",
		},
		{
			headings: []string{"Variable", "Type", "Description", "Code Position"},
			lengths:  []string{"----", "------", "--------", "------"},
			data: [][]string{
				[]string{"application_name", "string", "The Application name", "[variables.tf: 1](variables.tf#L1)"},
				[]string{"owner_obj_id", "string", "The owner object ID", "[variables.tf: 6](variables.tf#L6)"},
			},
			expected: "| Variable | Type | Description | Code Position |\n" +
				"| ---- | ------ | -------- | ------ |\n" +
				"| application_name | string | The Application name | [variables.tf: 1](variables.tf#L1) |\n" +
				"| owner_obj_id | string | The owner object ID | [variables.tf: 6](variables.tf#L6) |",
		},
	}
	for _, test := range tests {
		output := markdownTable(test.headings, test.lengths, test.data)
		if test.expected != output {
			t.Errorf("unexpected output: %s \n\n expected: %s ", output, test.expected)
		}
	}
}

//  test variables for reading from the mock terraform module in /examples
var (
	testModule, _ = tfconfig.LoadModule("../../examples/full_test")
	baseURL       = "testURL"
	modulePath    = "examples/full_test"
)

func TestGetOutputsTable(t *testing.T) {
	output := GetOutputsTable(testModule, baseURL, modulePath)
	headings := []string{"Output name", "Description", "Sensitive", "Code Position"}
	lengths := []string{"----", "--------", "----", "------"}
	data := [][]string{}
	var objs = make(map[string]tfTableObject)
	for _, item := range testModule.Outputs {
		tffile := getFilename(item.Pos.Filename)
		objs[item.Name] = tfTableObject{
			Name:        item.Name,
			Type:        strconv.FormatBool(item.Sensitive),
			Description: item.Description,
			Location:    formatLocationString(item.Pos.Line, tffile, baseURL, modulePath),
		}
	}
	for _, k := range getSortedKeys(objs) {
		data = append(data, []string{objs[k].Name, objs[k].Description, objs[k].Type, objs[k].Location})
	}
	if len(data) != len(testModule.Outputs) {
		t.Error("incorrect length of variables")
	}
	expectedOutput := markdownTable(headings, lengths, data)
	if output != expectedOutput {
		t.Errorf("expected:\n %s \n output:\n %s", expectedOutput, output)
	}
}

func TestGetVarsTable(t *testing.T) {
	output := GetVarsTable(testModule, baseURL, modulePath)
	headings := []string{"Variable", "Type", "Description", "Code Position"}
	lengths := []string{"----", "------", "--------", "------"}
	data := [][]string{}
	var objs = make(map[string]tfTableObject)
	for _, item := range testModule.Variables {
		tffile := getFilename(item.Pos.Filename)
		objs[item.Name] = tfTableObject{
			Name:        item.Name,
			Type:        markdownFormatType(item.Type),
			Description: item.Description,
			Location:    formatLocationString(item.Pos.Line, tffile, baseURL, modulePath),
		}
	}
	for _, k := range getSortedKeys(objs) {
		data = append(data, []string{objs[k].Name, objs[k].Type, objs[k].Description, objs[k].Location})
	}
	if len(data) != len(testModule.Variables) {
		t.Error("incorrect length of variables")
	}
	expectedOutput := markdownTable(headings, lengths, data)
	if output != expectedOutput {
		t.Errorf("expected:\n %s \n output:\n %s", expectedOutput, output)
	}
}

func TestGetProvidersTable(t *testing.T) {
	output := GetProvidersTable(testModule, baseURL, modulePath)
	headings := []string{"Name", "Version", "Code Postition"}
	lengths := []string{"----", "------", "------"}
	data := [][]string{}
	var objs = make(map[string]tfTableObject)
	for _, item := range loadProviders(testModule) {
		tffile := getFilename(item.Position.Filename)
		objs[item.Name] = tfTableObject{
			Name:        item.Name,
			Description: item.Version,
			Location:    formatLocationString(item.Position.Line, tffile, baseURL, modulePath),
		}
	}
	for _, k := range getSortedKeys(objs) {
		data = append(data, []string{objs[k].Name, objs[k].Description, objs[k].Location})
	}
	if len(data) != len(loadProviders(testModule)) {
		t.Error("invalid test setup step data length does not match expected provider list length")
	}
	expectedOutput := markdownTable(headings, lengths, data)
	if output != expectedOutput {
		t.Errorf("expected:\n %s \n output:\n %s", expectedOutput, output)
	}
}

func TestGetRequirementsTable(t *testing.T) {
	output := GetRequirementsTable(testModule, baseURL, modulePath)
	headings := []string{"Name", "Version"}
	lengths := []string{"----", "------"}
	data := [][]string{}
	var objs = make(map[string]tfTableObject)
	for _, item := range loadRequirements(testModule) {
		objs[item.Name] = tfTableObject{
			Name:        item.Name,
			Description: item.Version,
		}
	}
	for _, k := range getSortedKeys(objs) {
		data = append(data, []string{objs[k].Name, objs[k].Description})
	}
	if len(data) != len(loadRequirements(testModule)) {
		t.Error("invalid test setup step data length does not match expected provider list length")
	}
	expectedOutput := markdownTable(headings, lengths, data)
	if output != expectedOutput {
		t.Errorf("expected:\n %s \n output:\n %s", expectedOutput, output)
	}
}

func TestGetManagedResourcesTable(t *testing.T) {
	output := GetManagedResourcesTable(testModule, baseURL, modulePath)
	headings := []string{"Resource Name", "Resource Type", "Code Position"}
	lengths := []string{"----", "--------", "------"}
	data := [][]string{}

	var objs = make(map[string]tfTableObject) // Make a map of output objects
	for _, item := range testModule.ManagedResources {
		tffile := getFilename(item.Pos.Filename)
		objs[item.Name] = tfTableObject{
			Name:        item.Name,
			Type:        item.Type,
			Description: "",
			Location:    formatLocationString(item.Pos.Line, tffile, baseURL, modulePath),
		}
	}
	for _, k := range getSortedKeys(objs) {
		data = append(data, []string{objs[k].Name, objs[k].Type, objs[k].Location})
	}
	if len(data) != len(testModule.ManagedResources) {
		t.Error("invalid test setup step data length does not match expected resources list length")
	}
	expectedOutput := markdownTable(headings, lengths, data)
	if output != expectedOutput {
		t.Errorf("expected:\n %s \n output:\n %s", expectedOutput, output)
	}
	// TODO test code position location strings
}

func TestGetDataSourcesTable(t *testing.T) {
	output := GetDataSourcesTable(testModule, baseURL, modulePath)
	headings := []string{"Resource Name", "Resource Type", "Code Position"}
	lengths := []string{"----", "--------", "------"}
	data := [][]string{}

	var objs = make(map[string]tfTableObject) // Make a map of output objects
	for _, item := range testModule.DataResources {
		tffile := getFilename(item.Pos.Filename)
		objs[item.Name] = tfTableObject{
			Name:     item.Name,
			Type:     item.Type,
			Location: formatLocationString(item.Pos.Line, tffile, baseURL, modulePath),
		}
	}
	for _, k := range getSortedKeys(objs) {
		data = append(data, []string{objs[k].Name, objs[k].Type, objs[k].Location})
	}
	if len(data) != len(testModule.DataResources) {
		t.Error("invalid test setup step data length does not match expected data sources list length")
	}
	expectedOutput := markdownTable(headings, lengths, data)
	if output != expectedOutput {
		t.Errorf("expected:\n %s \n output:\n %s", expectedOutput, output)
	}
}

func TestGetModulesTable(t *testing.T) {
	output := GetModulesTable(testModule, baseURL, modulePath)
	headings := []string{"Module Name", "Module Source", "Module Location"}
	lengths := []string{"----", "--------", "------"}
	data := [][]string{}

	var objs = make(map[string]tfTableObject) // Make a map of output objects
	for _, item := range testModule.ModuleCalls {
		tffile := getFilename(item.Pos.Filename)
		objs[item.Name] = tfTableObject{
			Name:        item.Name,
			Type:        item.Source,
			Description: item.Version,
			Location:    formatLocationString(item.Pos.Line, tffile, baseURL, modulePath),
		}
	}
	for _, k := range getSortedKeys(objs) {
		data = append(data, []string{objs[k].Name, objs[k].Type, objs[k].Location})
	}
	if len(data) != len(testModule.ModuleCalls) {
		t.Error("invalid test setup step data length does not match expected data sources list length")
	}
	expectedOutput := markdownTable(headings, lengths, data)
	if output != expectedOutput {
		t.Errorf("expected:\n %s \n output:\n %s", expectedOutput, output)
	}
}

func TestBuildMarkdownToc(t *testing.T) {
	readmeTemplateBytes, err := ioutil.ReadFile("../../templates/update_readme.template.md")
	utils.CheckErr(err, "Failed to read template: %s")
	output, err := buildMarkdownToc(readmeTemplateBytes, 3, 0)
	if err != nil {
		t.Error(err)
	}
	expected := "Table of Contents\n=================\n" +
		"1. [Terraform Requirements](#terraform-requirements)\n" +
		"1. [Terraform Providers](#terraform-providers)\n" +
		"1. [Terraform Variables](#terraform-variables)\n" +
		"1. [Terraform Data Sources](#terraform-data-sources)\n" +
		"1. [Terraform Resources](#terraform-resources)\n" +
		"1. [Terraform Modules](#terraform-modules)\n" +
		"1. [Terraform Outputs](#terraform-outputs)"
	formatted := strings.Join(output, "\n")
	if formatted != expected {
		t.Errorf("expected:\n %s \n output:\n %s", expected, formatted)
	}
}

func TestFormatLocationString(t *testing.T) {
	tests := []struct {
		pos        int
		filename   string
		modulePath string
		baseURL    string
		expected   string
	}{
		{
			pos:        1,
			filename:   "testFile",
			modulePath: "",
			baseURL:    "testURL",
			expected:   "[testFile: 1](testURL/testFile#L1)",
		},
		{
			pos:        5,
			filename:   "testFile2",
			modulePath: "",
			baseURL:    "",
			expected:   "[testFile2: 5](testFile2#L5)",
		},
		{
			pos:        8,
			filename:   "testFile2",
			modulePath: "examples",
			baseURL:    "testURL",
			expected:   "[testFile2: 8](testURL/examples/testFile2#L8)",
		},
		{
			pos:        8,
			filename:   "testmain2.tf",
			modulePath: "examples",
			baseURL:    "",
			expected:   "[testmain2.tf: 8](/examples/testmain2.tf#L8)",
		},
	}
	for _, test := range tests {
		output := formatLocationString(test.pos, test.filename, test.baseURL, test.modulePath)
		if output != test.expected {
			t.Errorf("expected:\n %s \n output:\n %s", test.expected, output)
		}
	}
}

func TestLoadProviders(t *testing.T) {
	type expected struct {
		providers int
	}
	tests := []struct {
		name     string
		path     string
		expected int
	}{
		{
			name:     "load module providers from path",
			path:     "full_test",
			expected: 4,
		},
		{
			name:     "load module providers from module with no providers",
			path:     "no_providers",
			expected: 0,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			module, _ := tfconfig.LoadModule(filepath.Join("../../examples", tt.path))
			providers := loadProviders(module)

			if tt.expected != len(providers) {
				t.Errorf("unexpected number of providers loaded %d", len(providers))
			}
		})
	}
}

func TestLoadRequirements(t *testing.T) {
	type expected struct {
		providers int
	}
	tests := []struct {
		name     string
		path     string
		expected int
	}{
		{
			name:     "load module requirements from path",
			path:     "full_test",
			expected: 5,
		},
		{
			name:     "load module requirements from module with no requirements",
			path:     "no_providers",
			expected: 0,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			module, _ := tfconfig.LoadModule(filepath.Join("../../examples", tt.path))
			requirements := loadRequirements(module)

			if tt.expected != len(requirements) {
				t.Errorf("unexpected number of providers loaded %d", len(requirements))
			}
		})
	}
}

func TestGetFilename(t *testing.T) {
	tests := []struct {
		filepath       string
		expectedOutput string
	}{
		{
			filepath:       "a/test/string/main.tf",
			expectedOutput: "main.tf",
		},
		{
			filepath:       "a/new/test.md",
			expectedOutput: "test.md",
		},
		{
			filepath:       "../",
			expectedOutput: "",
		},
	}
	for _, test := range tests {
		output := getFilename(test.filepath)
		if output != test.expectedOutput {
			t.Errorf("expected:\n %s \n output:\n %s", test.expectedOutput, output)
		}
	}

}
