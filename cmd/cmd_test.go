package cmd

import (
	"flag"
	"log"
	"os"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestParseCli(t *testing.T) {
	tests := []struct {
		Args        []string
		Output      CliOpts
		HasError    bool
		Description string
	}{
		{
			Args:        []string{"-path", "../examples/full_test", "-action", "OutputsTable", "-modulePath", ""},
			Output:      CliOpts{TfPath: "../examples/full_test", Action: "OutputsTable"},
			HasError:    false,
			Description: "Valid path with Module path empty and action of outputs table ",
		},
		{
			Args:        []string{"-path", "", "-action", "OutputsTable"},
			Output:      CliOpts{Action: "OutputsTable"},
			HasError:    true,
			Description: "No path",
		},
		{
			Args:        []string{"-path", "test/path"},
			Output:      CliOpts{TfPath: "test/path"},
			HasError:    true,
			Description: "No Action",
		},
		{
			Args:        []string{"-path", "test/path", "-action", "NotAnAction"},
			Output:      CliOpts{TfPath: "test/path", Action: "NotAnAction"},
			HasError:    true,
			Description: "Invalid Action",
		},
		{
			Args:        []string{"-path", "test/path", "-action", "RenderTemplate", "-templatePath", "dir/template.md"},
			Output:      CliOpts{TfPath: "test/path", Action: "RenderTemplate", TemplatePath: "dir/template.md"},
			HasError:    false,
			Description: "Render Template",
		},
		{
			Args:        []string{"-path", "test/path", "-action", "RenderTemplate"},
			Output:      CliOpts{TfPath: "test/path", Action: "RenderTemplate"},
			HasError:    true,
			Description: "Render Template no template path",
		},
		{
			Args:        []string{"-path", "test/path", "-action", "UpdateFile", "-outputPath", "readme.md"},
			Output:      CliOpts{TfPath: "test/path", Action: "UpdateFile", OutputPath: "readme.md"},
			HasError:    true,
			Description: "Update file no template path",
		},
		{
			Args: []string{
				"-path", "test/path", "-action", "UpdateFile",
				"-outputPath", "readme.md", "-templatePath", "template.md",
			},
			Output:      CliOpts{TfPath: "test/path", Action: "UpdateFile", OutputPath: "readme.md", TemplatePath: "template.md"},
			HasError:    false,
			Description: "Update file",
		},
		{
			Args:        []string{"-path", "test/path", "-action", "UpdateFile", "-templatePath", "template.md"},
			Output:      CliOpts{TfPath: "test/path", Action: "UpdateFile", TemplatePath: "template.md"},
			HasError:    true,
			Description: "Update file no output path",
		},
	}
	originalArg := os.Args[0]
	for _, test := range tests {
		t.Run(test.Description, func(t *testing.T) {
			flagset := flag.FlagSet{}
			baseArgs := []string{originalArg}
			newArgs := append(baseArgs, test.Args...)
			os.Args = newArgs
			opts, err := ParseCli(&flagset)
			if err != nil && !test.HasError {
				t.Error(err)
			}
			if err == nil && test.HasError {
				t.Error("expected error")
			}
			assert.Equal(t, opts, &test.Output)
		})

	}
}

func TestExecute(t *testing.T) {
	tests := []struct {
		Args        []string
		HasError    bool
		Description string
	}{
		{
			Args:        []string{"-path", "../examples/full_test", "-action", "OutputsTable", "-modulePath", ""},
			HasError:    false,
			Description: "Valid path with Module path empty and action of outputs table ",
		},
		{
			Args:        []string{"-path", "../examples/not_a_module", "-action", "OutputsTable"},
			HasError:    true,
			Description: "Invalid path",
		},
		{
			Args:        []string{"-path", "../examples/full_test", "-action", "random"},
			HasError:    true,
			Description: "Invalid action ",
		},
		{
			Args:        []string{"-path", "../examples/full_test", "-action", "UpdateFile", "-templatePath", "template.md", "outputPath", "readme.md"},
			HasError:    true,
			Description: "Update file fake template path",
		},
		{
			Args: []string{
				"-path", "../examples/full_test", "-action", "UpdateFile",
				"-outputPath", "readme.md", "-templatePath", "../templates/update_readme.template.md",
			},
			HasError:    false,
			Description: "Update file valid template path",
		},
		{
			Args: []string{
				"-path", "../examples/full_test", "-action", "RenderTemplate",
				"-templatePath", "../templates/update_readme.template.md",
			},
			HasError:    false,
			Description: "Render Template",
		},
		{
			Args: []string{
				"-path", "../examples/full_test", "-action", "RenderTemplate",
				"-templatePath", "../not_templates/update_readme.template.md",
			},
			HasError:    true,
			Description: "Render Template Invalid",
		},
	}
	os.Create("readme.md")
	originalArg := os.Args[0]
	for _, test := range tests {
		t.Run(test.Description, func(t *testing.T) {
			log.Println(test.Description)
			baseArgs := []string{originalArg}
			newArgs := append(baseArgs, test.Args...)
			os.Args = newArgs
			if test.HasError {
				assert.Panics(t, assert.PanicTestFunc(Execute))
			} else {
				assert.NotPanics(t, assert.PanicTestFunc(Execute))
			}
		})
	}
	os.Remove("readme.md")
}
