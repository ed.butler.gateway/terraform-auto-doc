package tables

import (
	"github.com/hashicorp/terraform-config-inspect/tfconfig"
	"github.com/stretchr/testify/assert"
	"io/ioutil"
	"strings"
	"testing"
	"text/template"
)

// TODO future versions should catch errors outside of CheckErr method to allow deeper testing
func TestRenderTemplateData(t *testing.T) {
	tests := []struct {
		templatePath string
		tfPath       string
		hasError     bool
	}{
		{
			templatePath: "../../templates/output_and_vars.template.md",
			tfPath:       "../../examples/full_test",
			hasError:     false,
		},
		{
			templatePath: "../../templates/not_a_template",
			tfPath:       "../../examples/full_test",
			hasError:     true,
		},
		{
			templatePath: "../../templates/not_a_template",
			tfPath:       "../../examples/not_a_module",
			hasError:     true,
		},
		{
			templatePath: "../../templates/terraform_module_doc.template.md",
			tfPath:       "../../examples/no_providers",
			hasError:     false,
		},
	}
	repoURL := "testURL"
	modulePath := ""
	for _, test := range tests {
		testModule, _ := tfconfig.LoadModule(test.tfPath)
		templatePath := test.templatePath
		data, outTemplate, outputErr := RenderTemplateData(templatePath, repoURL, modulePath, testModule)
		if test.hasError {
			if outputErr == nil {
				t.Error("expected render template method to error")
			}
			assert.Equal(t, data, TemplateData{})
			assert.Equal(t, outTemplate, &template.Template{})
		} else {
			if outputErr == nil {
				output := ""
				for _, node := range outTemplate.Tree.Root.Nodes {
					output = output + node.String()
				}
				templateBytes, _ := ioutil.ReadFile(templatePath)
				expTemplate := string(templateBytes)
				if output != string(templateBytes) {
					t.Errorf("expected:\n %s \n output:\n %s", expTemplate, output)
				}
				readmeTemplateBytes, _ := ioutil.ReadFile(templatePath)
				toc, _ := buildMarkdownToc(readmeTemplateBytes, 3, 0)
				expectedData := TemplateData{
					TerraformOutputsTable:          GetOutputsTable(testModule, repoURL, modulePath),
					TerraformRequirementsTable:     GetRequirementsTable(testModule, repoURL, modulePath),
					TerraformProvidersTable:        GetProvidersTable(testModule, repoURL, modulePath),
					TerraformVarsTable:             GetVarsTable(testModule, repoURL, modulePath),
					TerraformManagedResourcesTable: GetManagedResourcesTable(testModule, repoURL, modulePath),
					TerraformDataSourcesTable:      GetDataSourcesTable(testModule, repoURL, modulePath),
					TerraformModulesTable:          GetModulesTable(testModule, repoURL, modulePath),
					MarkdownTOC:                    strings.Join(toc, "\n"),
					RepoBaseURL:                    repoURL,
				}
				assert.EqualValues(t, expectedData, data)
			} else {
				t.Errorf("unexpected error in the render template method %s", outputErr.Error())
			}
		}
	}
}
