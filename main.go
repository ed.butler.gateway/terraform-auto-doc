package main

import (
	"terraform-auto-doc/cmd"
)

func main() {
	cmd.Execute()
}
