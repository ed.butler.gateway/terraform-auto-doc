package tables

import (
	"bufio"
	"bytes"
	"fmt"
	"github.com/hashicorp/terraform-config-inspect/tfconfig"
	"regexp"
	"sort"
	"strconv"
	"strings"

	utils "terraform-auto-doc/internal/utils"
)

type tfTableObject struct {
	Name, Type, Description, Location string
}

type position struct {
	Filename string
	Line     int
}

type requirement struct {
	Name    string `json:"name" xml:"name" yaml:"name"`
	Version string `json:"version" xml:"version" yaml:"version"`
}

type provider struct {
	Name     string
	Alias    string
	Version  string
	Position position
}

var (
	rHashHeader        = regexp.MustCompile("^(?P<indent>#+) ?(?P<title>.+)$")
	rUnderscoreHeader1 = regexp.MustCompile("^=+$")
	rUnderscoreHeader2 = regexp.MustCompile("^\\-+$")
)

func loadRequirements(tfmodule *tfconfig.Module) []*requirement {
	var requirements = make([]*requirement, 0)
	for _, core := range tfmodule.RequiredCore {
		requirements = append(requirements, &requirement{
			Name:    "terraform",
			Version: string(core),
		})
	}
	names := make([]string, 0, len(tfmodule.RequiredProviders))
	for n := range tfmodule.RequiredProviders {
		names = append(names, n)
	}
	sort.Strings(names)
	for _, name := range names {
		for _, version := range tfmodule.RequiredProviders[name].VersionConstraints {
			requirements = append(requirements, &requirement{
				Name:    name,
				Version: string(version),
			})
		}
	}
	return requirements
}

func loadProviders(tfmodule *tfconfig.Module) []*provider {
	resources := []map[string]*tfconfig.Resource{tfmodule.ManagedResources, tfmodule.DataResources}
	discovered := make(map[string]*provider)
	for _, resource := range resources {
		for _, r := range resource {
			var version = ""
			if rv, ok := tfmodule.RequiredProviders[r.Provider.Name]; ok && len(rv.VersionConstraints) > 0 {
				version = strings.Join(rv.VersionConstraints, " ")
			}
			key := fmt.Sprintf("%s.%s", r.Provider.Name, r.Provider.Alias)
			discovered[key] = &provider{
				Name:    r.Provider.Name,
				Alias:   string(r.Provider.Alias),
				Version: string(version),
				Position: position{
					Filename: r.Pos.Filename,
					Line:     r.Pos.Line,
				},
			}
		}
	}
	providers := make([]*provider, 0, len(discovered))
	for _, prov := range discovered {
		providers = append(providers, prov)
	}
	return providers
}

func markdownTableCellEscape(cellText string) string {
	// Replace Newlines with <br/>
	re := regexp.MustCompile(`\r?\n`)
	cellText = re.ReplaceAllString(cellText, "<br>")
	return cellText
}

func markdownFormatType(input string) string {
	// Wrap type declarations with a <pre> tag
	if input == "" {
		return input
	}
	return fmt.Sprintf("<pre> %s </pre>", input)
}

func markdownTable(headings []string, lengths []string, data [][]string) string {
	// TODO - input/parameter validation
	// if len(headings) != len(lengths) {
	// 	utils.CheckErr(errors.New(""), "length of headings and specified length slices do not match")
	// }
	// if len(data) != 0 && len(data[0]) != len(headings) {
	// 	utils.CheckErr(errors.New(""), "length of headings and specified length slices do not match")
	// }
	table := "|"
	for _, h := range headings {
		table += fmt.Sprintf(" %s |", h)
	}
	table += "\n|"
	for _, l := range lengths {
		table += fmt.Sprintf(" %s |", l)
	}

	for _, d := range data {
		table += "\n|"
		for _, val := range d {
			table += fmt.Sprintf(" %s |", markdownTableCellEscape(val))
		}
	}

	return table

}

func getFilename(filename string) string {
	filepathbits := strings.Split(filename, "/")
	return filepathbits[len(filepathbits)-1]
}

func GetRequirementsTable(module *tfconfig.Module, baseURL, modulePath string) string {
	headings := []string{"Name", "Version"}
	lengths := []string{"----", "------"}
	data := [][]string{}
	var objs = make(map[string]tfTableObject)
	for _, item := range loadRequirements(module) {
		objs[item.Name] = tfTableObject{
			Name:        item.Name,
			Description: item.Version,
		}
	}
	for _, k := range getSortedKeys(objs) {
		data = append(data, []string{objs[k].Name, objs[k].Description})
	}
	return markdownTable(headings, lengths, data)
}

func GetProvidersTable(module *tfconfig.Module, baseURL, modulePath string) string {
	headings := []string{"Name", "Version", "Code Postition"}
	lengths := []string{"----", "------", "------"}
	data := [][]string{}
	// Make a map of item objects
	var objs = make(map[string]tfTableObject)
	for _, item := range loadProviders(module) {
		tffile := getFilename(item.Position.Filename)
		objs[item.Name] = tfTableObject{
			Name:        item.Name,
			Description: item.Version,
			Location:    formatLocationString(item.Position.Line, tffile, baseURL, modulePath),
		}
	}
	for _, k := range getSortedKeys(objs) {
		data = append(data, []string{objs[k].Name, objs[k].Description, objs[k].Location})
	}

	return markdownTable(headings, lengths, data)
}

func GetVarsTable(module *tfconfig.Module, baseURL, modulePath string) string {
	headings := []string{"Variable", "Type", "Description", "Code Position"}
	lengths := []string{"----", "------", "--------", "------"}
	data := [][]string{}

	// Make a map of item objects
	var objs = make(map[string]tfTableObject)
	for _, item := range module.Variables {
		tffile := getFilename(item.Pos.Filename)
		objs[item.Name] = tfTableObject{
			Name:        item.Name,
			Type:        markdownFormatType(item.Type),
			Description: item.Description,
			Location:    formatLocationString(item.Pos.Line, tffile, baseURL, modulePath),
		}
	}
	for _, k := range getSortedKeys(objs) {
		data = append(data, []string{objs[k].Name, objs[k].Type, objs[k].Description, objs[k].Location})
	}
	return markdownTable(headings, lengths, data)
}

func GetOutputsTable(module *tfconfig.Module, baseURL, modulePath string) string {
	headings := []string{"Output name", "Description", "Sensitive", "Code Position"}
	lengths := []string{"----", "--------", "----", "------"}
	data := [][]string{}
	// TODO sort type on outputs
	var objs = make(map[string]tfTableObject) // Make a map of output objects
	for _, item := range module.Outputs {
		tffile := getFilename(item.Pos.Filename)
		objs[item.Name] = tfTableObject{
			Name:        item.Name,
			Type:        strconv.FormatBool(item.Sensitive),
			Description: item.Description,
			Location:    formatLocationString(item.Pos.Line, tffile, baseURL, modulePath),
		}
	}

	for _, k := range getSortedKeys(objs) {
		data = append(data, []string{objs[k].Name, objs[k].Description, objs[k].Type, objs[k].Location})
	}
	return markdownTable(headings, lengths, data)
}

func GetManagedResourcesTable(module *tfconfig.Module, baseURL, modulePath string) string {
	headings := []string{"Resource Name", "Resource Type", "Code Position"}
	lengths := []string{"----", "--------", "------"}
	data := [][]string{}
	var objs = make(map[string]tfTableObject) // Make a map of output objects
	for _, item := range module.ManagedResources {
		tffile := getFilename(item.Pos.Filename)
		objs[item.Name] = tfTableObject{
			Name:     item.Name,
			Type:     item.Type,
			Location: formatLocationString(item.Pos.Line, tffile, baseURL, modulePath),
		}
	}
	for _, k := range getSortedKeys(objs) {
		data = append(data, []string{objs[k].Name, objs[k].Type, objs[k].Location})
	}
	return markdownTable(headings, lengths, data)
}

func GetDataSourcesTable(module *tfconfig.Module, baseURL, modulePath string) string {
	headings := []string{"Resource Name", "Resource Type", "Code Position"}
	lengths := []string{"----", "--------", "------"}
	data := [][]string{}

	var objs = make(map[string]tfTableObject) // Make a map of output objects
	for _, item := range module.DataResources {
		tffile := getFilename(item.Pos.Filename)
		objs[item.Name] = tfTableObject{
			Name:     item.Name,
			Type:     item.Type,
			Location: formatLocationString(item.Pos.Line, tffile, baseURL, modulePath),
		}
	}
	for _, k := range getSortedKeys(objs) {
		data = append(data, []string{objs[k].Name, objs[k].Type, objs[k].Location})
	}
	return markdownTable(headings, lengths, data)
}

func GetModulesTable(module *tfconfig.Module, baseURL, modulePath string) string {
	headings := []string{"Module Name", "Module Source", "Module Location"}
	lengths := []string{"----", "--------", "------"}
	data := [][]string{}

	var objs = make(map[string]tfTableObject) // Make a map of output objects
	for _, item := range module.ModuleCalls {
		tffile := getFilename(item.Pos.Filename)
		objs[item.Name] = tfTableObject{
			Name:        item.Name,
			Type:        item.Source,
			Description: item.Version,
			Location:    formatLocationString(item.Pos.Line, tffile, baseURL, modulePath),
		}
	}
	for _, k := range getSortedKeys(objs) {
		data = append(data, []string{objs[k].Name, objs[k].Type, objs[k].Location})
	}
	return markdownTable(headings, lengths, data)
}

func getSortedKeys(objs map[string]tfTableObject) []string {
	keys := make([]string, 0, len(objs))
	for k := range objs {
		keys = append(keys, k)
	}
	sort.Strings(keys)
	return keys
}

func formatLocationString(linePosition int, fileName, baseURL, relativePath string) string {
	if relativePath != "" && baseURL != "" {
		return fmt.Sprintf("[%s: %d](%s/%s/%s#L%d)", fileName, linePosition, baseURL, relativePath, fileName, linePosition)
	}
	if relativePath != "" && baseURL == "" {
		return fmt.Sprintf("[%s: %d](/%s/%s#L%d)", fileName, linePosition, relativePath, fileName, linePosition)
	}
	if relativePath == "" && baseURL == "" {
		return fmt.Sprintf("[%s: %d](%s#L%d)", fileName, linePosition, fileName, linePosition)
	}
	return fmt.Sprintf("[%s: %d](%s/%s#L%d)", fileName, linePosition, baseURL, fileName, linePosition)
}

//  https://github.com/sebdah/markdown-toc/tree/master/toc
func buildMarkdownToc(d []byte, depth, skipHeaders int) ([]string, error) {
	toc := []string{
		"Table of Contents",
		"=================",
	}

	seenHeaders := make(map[string]int)
	var previousLine string
	appendToC := func(title string, indent int) {
		link := utils.Slugify(title)
		if skipHeaders > 0 {
			skipHeaders--
			return
		}

		if _, ok := seenHeaders[link]; ok {
			seenHeaders[link]++
			link = fmt.Sprintf("%s-%d", link, seenHeaders[link]-1)
		} else {
			seenHeaders[link] = 1
		}
		toc = append(toc, fmt.Sprintf("%s1. [%s](#%s)", strings.Repeat("   ", indent), title, link))
	}

	s := bufio.NewScanner(bytes.NewReader(d))
	for s.Scan() {
		switch {
		case rHashHeader.Match(s.Bytes()):
			m := rHashHeader.FindStringSubmatch(s.Text())
			if depth > 0 && len(m[1]) > depth {
				continue
			}
			appendToC(m[2], len(m[1])-1)

		case rUnderscoreHeader1.Match(s.Bytes()):
			appendToC(previousLine, 0)

		case rUnderscoreHeader2.Match(s.Bytes()):
			if depth > 0 && depth < 2 {
				continue
			}
			appendToC(previousLine, 1)
		}
		previousLine = s.Text()
	}
	if err := s.Err(); err != nil {
		return []string{}, err
	}

	return toc, nil
}
