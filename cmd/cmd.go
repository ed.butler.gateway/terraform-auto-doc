package cmd

// TODO - better error handling (remove all panics)

import (
	"bytes"
	"errors"
	"flag"
	"fmt"
	"io/ioutil"
	"os"
	"regexp"

	"github.com/hashicorp/terraform-config-inspect/tfconfig"

	tables "terraform-auto-doc/internal/tables"
	utils "terraform-auto-doc/internal/utils"
)

var generationText = "generation by terraform auto doc"
var reUpdateFile = regexp.MustCompile(fmt.Sprintf(`(?s)<!-- %s -->(.*?)\n<!-- %s end -->`, generationText, generationText))

var ValidActions = []string{
	"VarsTable",
	"OutputsTable",
	"ManagedResourcesTable",
	"DataSourcesTable",
	"ProvidersTable",
	"RequirementsTable",
	"RenderTemplate",
	"UpdateFile",
}

type CliOpts struct {
	TfPath       string
	Action       string
	TemplatePath string
	OutputPath   string
	RepoURL      string
	ModulePath   string
}

// ParseCli recieves a *flag.Flagset and parses the flags used in the main execution method returns the parsed options CliOpts
//  Error if the flags are unable to be parsed or an invalid combination of actions
func ParseCli(newFlags *flag.FlagSet) (*CliOpts, error) {
	opts := CliOpts{}
	tfPathPtr := newFlags.String("path", "", "The path to the Terraform Module to inspect.")
	outputPathPtr := newFlags.String("outputPath", "", "The path to the file to update.")
	actionPtr := newFlags.String("action", "", fmt.Sprintf("The Action to perform. %s", ValidActions))
	templatePathPtr := newFlags.String("templatePath", "", "The path to the template to render")
	repoURLPtr := newFlags.String("repoURL", "", "The URL path used as a prefix for links")
	modulePathPtr := newFlags.String("modulePath", "", "The path of the module relative to the repository")
	err := newFlags.Parse(os.Args[1:])
	if err != nil {
		return &opts, err
	}
	opts.TfPath = *tfPathPtr
	opts.Action = *actionPtr
	opts.TemplatePath = *templatePathPtr
	opts.RepoURL = *repoURLPtr
	opts.ModulePath = *modulePathPtr
	opts.OutputPath = *outputPathPtr

	if opts.TfPath == "" {
		return &opts, errors.New("no TF Path set")
	}
	if opts.Action == "" {
		return &opts, errors.New("No Action set")
	}
	if !utils.StringInSlice(opts.Action, ValidActions) {
		return &opts, fmt.Errorf("Action %s is not one of: %s", opts.Action, ValidActions)
	}
	if (opts.Action == "RenderTemplate" || opts.Action == "UpdateFile") && opts.TemplatePath == "" {
		return &opts, errors.New("no Template path specified")
	}

	if opts.Action == "UpdateFile" && opts.OutputPath == "" {
		return &opts, errors.New("no output path specified for file to update")
	}

	return &opts, nil
}

// Execute will parse flags and options from the cmd client and run the appropriate scripts
// if an error is flagged in this method the package with exit
func Execute() {
	flagset := flag.FlagSet{}
	cliOpts, err := ParseCli(&flagset)
	if err != nil {
		flagset.PrintDefaults()
		panic("failed to parse flags")
	}

	module, diags := tfconfig.LoadModule(cliOpts.TfPath)
	if diags.HasErrors() {
		panic("Problem Loading Module: " + diags.Error())
	}

	if cliOpts.Action == "VarsTable" {
		fmt.Println(tables.GetVarsTable(module, cliOpts.RepoURL, cliOpts.ModulePath))
	} else if cliOpts.Action == "OutputsTable" {
		fmt.Println(tables.GetOutputsTable(module, cliOpts.RepoURL, cliOpts.ModulePath))
	} else if cliOpts.Action == "ManagedResourcesTable" {
		fmt.Println(tables.GetRequirementsTable(module, cliOpts.RepoURL, cliOpts.ModulePath))
	} else if cliOpts.Action == "ProvidersTable" {
		fmt.Println(tables.GetProvidersTable(module, cliOpts.RepoURL, cliOpts.ModulePath))
	} else if cliOpts.Action == "RequirementsTable" {
		fmt.Println(tables.GetManagedResourcesTable(module, cliOpts.RepoURL, cliOpts.ModulePath))
	} else if cliOpts.Action == "RenderTemplate" {
		data, t, err := tables.RenderTemplateData(cliOpts.TemplatePath, cliOpts.RepoURL, cliOpts.ModulePath, module)
		if err != nil {
			panic(err)
		}
		err = t.Execute(os.Stdout, data)
		if err != nil {
			panic(fmt.Errorf("failed rendering template: %s", cliOpts.TemplatePath))
		}
	} else if cliOpts.Action == "UpdateFile" {
		data, t, err := tables.RenderTemplateData(cliOpts.TemplatePath, cliOpts.RepoURL, cliOpts.ModulePath, module)
		if err != nil {
			panic(err)
		}
		var buf bytes.Buffer
		err = t.Execute(&buf, data)
		if err != nil {
			panic(fmt.Errorf("failed rendering template: %s", cliOpts.TemplatePath))
		}
		input, err := ioutil.ReadFile(cliOpts.OutputPath)
		if err != nil {
			panic(err)
		}
		updatedFile := reUpdateFile.ReplaceAll(input, buf.Bytes())
		if !reUpdateFile.Match(input) {
			updatedFile = append(updatedFile, []byte("\n")...)
			updatedFile = append(updatedFile, buf.Bytes()...)
		}
		err = ioutil.WriteFile(cliOpts.OutputPath, []byte(updatedFile), os.ModePerm)
		if err != nil {
			panic(err)
		}
	} else {
		utils.CheckErr(fmt.Errorf("Action %s not implented yet", cliOpts.Action), "Action not implented yet")
	}
}
