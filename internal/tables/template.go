package tables

import (
	"io/ioutil"
	"path"
	"strings"
	"text/template"

	"github.com/hashicorp/terraform-config-inspect/tfconfig"
)

type TemplateData struct {
	TerraformVarsTable             string
	TerraformRequirementsTable     string
	TerraformProvidersTable        string
	TerraformOutputsTable          string
	TerraformManagedResourcesTable string
	TerraformDataSourcesTable      string
	TerraformModulesTable          string
	MarkdownTOC                    string
	RepoBaseURL                    string
}

func RenderTemplateData(templatePath, repoURL, modulePath string, module *tfconfig.Module) (TemplateData, *template.Template, error) {
	// Load the template
	name := path.Base(templatePath)
	t, err := template.New(name).Funcs(template.FuncMap{
		"rawfile": func(filepath string) (string, error) {
			parent := path.Dir(templatePath)
			rawFilePath := parent + "/" + filepath
			fileBytes, err := ioutil.ReadFile(rawFilePath)
			return string(fileBytes), err
		},
	}).ParseFiles(templatePath)
	if err != nil {
		return TemplateData{}, &template.Template{}, err
	}
	readmeTemplateBytes, err := ioutil.ReadFile(templatePath)
	if err != nil {
		return TemplateData{}, &template.Template{}, err
	}
	toc, err := buildMarkdownToc(readmeTemplateBytes, 3, 0)
	if err != nil {
		return TemplateData{}, &template.Template{}, err
	}
	return TemplateData{
		TerraformOutputsTable:          GetOutputsTable(module, repoURL, modulePath),
		TerraformRequirementsTable:     GetRequirementsTable(module, repoURL, modulePath),
		TerraformProvidersTable:        GetProvidersTable(module, repoURL, modulePath),
		TerraformVarsTable:             GetVarsTable(module, repoURL, modulePath),
		TerraformManagedResourcesTable: GetManagedResourcesTable(module, repoURL, modulePath),
		TerraformDataSourcesTable:      GetDataSourcesTable(module, repoURL, modulePath),
		TerraformModulesTable:          GetModulesTable(module, repoURL, modulePath),
		MarkdownTOC:                    strings.Join(toc, "\n"),
		RepoBaseURL:                    repoURL,
	}, t, nil

}
