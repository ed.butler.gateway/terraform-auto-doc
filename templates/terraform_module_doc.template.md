{{.MarkdownTOC}}



# Terraform Variables

{{.TerraformVarsTable}}

# Terraform Data Sources

{{.TerraformDataSourcesTable}}

# Terraform Managed resources

{{.TerraformManagedResourcesTable}}

# Terraform Modules

{{.TerraformModulesTable}}

# Terraform Outputs

{{.TerraformOutputsTable}}
