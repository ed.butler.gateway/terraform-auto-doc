package utils

import (
	"log"
	"os"
	"strings"
)

var stderr = log.New(os.Stderr, "", 1)

func StringInSlice(a string, list []string) bool {
	for _, b := range list {
		if b == a {
			return true
		}
	}
	return false
}

func CheckErr(e error, msg string) {
	if e != nil {
		if msg != "" {
			stderr.Println(msg)
		}
		stderr.Println(e.Error())
		os.Exit(1)
	}
}

//  Slugufy came from: https://github.com/sebdah/markdown-toc/tree/master/toc
func Slugify(s string) string {
	droppedChars := []string{
		"\"", "'", "`", ".",
		"!", ",", "~", "&",
		"%", "^", "*", "#",
		"@", "|",
		"(", ")",
		"{", "}",
		"[", "]",
	}

	s = strings.ToLower(s)
	for _, c := range droppedChars {
		s = strings.Replace(s, c, "", -1)
	}

	s = strings.Replace(s, " ", "-", -1)

	return s
}